﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace VotersApp
{
    class Program //VoterClient
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Voting app\n");

                Vote().Wait();
                Console.ReadKey();
                Console.Clear();
            }
        }

        static async Task Vote()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:53175/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response;

                Console.WriteLine("Candidates:");

                response = await client.GetAsync("api/Candidate/results");
                if (response.IsSuccessStatusCode)
                {
                    Candidate[] reports = await response.Content.ReadAsAsync<Candidate[]>();
                    foreach (var report in reports)
                    {
                        Console.WriteLine($"{report.Name}");
                    }
                }

            }

            Vote newVote = new Vote();
            Console.WriteLine("\n\nEnter Pesel: (dla ułatwienia obsługi 3 liczby!)");  //zmiana ewentualnej długości w VoteController
            newVote.PESEL = Console.ReadLine();
            Console.WriteLine("Enter Candidate Name:");
            newVote.VoteOnCandidate = Console.ReadLine();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:53175/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.PostAsJsonAsync("api/Vote", newVote);
                ;
                if (response.IsSuccessStatusCode)
                {
                    bool result = await response.Content.ReadAsAsync<bool>();
                    if (result)
                        Console.WriteLine("Vote Submitted");
                    else
                        Console.WriteLine("An error has occurred, try again.");
                }
            }

        }
    }
}
