﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VotingService.Models;

namespace VotingService.Controllers
{
    public class VoteController : ApiController
    {
        public static List<Vote> votes = new List<Vote>();

        [Route("api/Vote/results")]
        [HttpGet]
        public List<Vote> Get()
        {
            return votes;
        }

        [HttpPost]
        public bool Post(Vote newVote)
        {
            foreach (var vote in votes)
            {
                if (newVote.PESEL == vote.PESEL)
                {
                    return false;
                }
            }

            if (newVote.PESEL.Length != 3)
            {
                return false;
            }

            try
            {
                votes.Add(newVote);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public void Delete()
        {
            votes = new List<Vote>();
        }
    }
}
