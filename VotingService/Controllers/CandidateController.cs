﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VotingService.Models;

namespace VotingService.Controllers
{
    public class CandidateController : ApiController
    {
        public static List<Candidate> candidates = new List<Candidate>()
        {
            new Candidate {Name = "Kowalski"},
            new Candidate {Name = "Nowak"},
            new Candidate {Name = "Janowski"},
        };

        [Route("api/Candidate/results")]
        [HttpGet]
        public List<Candidate> Get()
        {
            return candidates;
        }

        [HttpPost]
        public bool Post(Candidate newCandidate)
        {
            try
            {
                candidates.Add(newCandidate);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
