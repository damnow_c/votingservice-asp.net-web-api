﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VotingService.Models
{
    public class Vote
    {
        public string PESEL { get; set; }
        public string VoteOnCandidate { get; set; }
    }
}