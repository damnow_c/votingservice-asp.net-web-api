﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Web.Script.Serialization;

namespace CommisionApp
{
    public class VoteOperationsJSON
    {
        static string jsonFile = Directory.GetCurrentDirectory();

        public static void SerializeAllVotes(List<Vote> votes)
        {
            using (StreamWriter file = File.CreateText(jsonFile + "allVotes.json"))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(file, votes);
            }
        }

        public static List<Vote> DeserializeAllVotes()
        {
            string json = File.ReadAllText(jsonFile  + "allVotes.json");
            JavaScriptSerializer js = new JavaScriptSerializer();
            var jsonObject = js.Deserialize<List<Vote>>(json);
            return jsonObject;
        }
    }
}
