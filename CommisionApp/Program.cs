﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace CommisionApp
{
    class Program
    {
        static List<CandidateResult> candidateResults = new List<CandidateResult>();
        static List<Candidate> candidates = new List<Candidate>();
        static List<Vote> votes = new List<Vote>();

        public static void LoadAddedVotes(List<Vote> votesJSON)
        {
            foreach (var vote in votesJSON)
            {
                votes.Add(vote);
            }
        }

        static void Main(string[] args)
        {
            try
            {
                LoadAddedVotes(VoteOperationsJSON.DeserializeAllVotes());
            }
            catch (Exception e)
            {
                Console.WriteLine("No initial votes load!");
            }

            while (true)
            {
                Console.WriteLine("Results app\n");

                GetResults().Wait();

                System.Threading.Thread.Sleep(5000);

                Console.Clear();
            }
        }

        static async Task GetResults()
        {
            candidateResults = new List<CandidateResult>();
            candidates = new List<Candidate>();
            bool alreadyVoted = false;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:53175/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response;

                Console.WriteLine("Candidate results:");

                response = await client.GetAsync("api/Candidate/results");
                if (response.IsSuccessStatusCode)
                {
                    Candidate[] reports = await response.Content.ReadAsAsync<Candidate[]>();

                    candidates = reports.ToList();
                }

                foreach (var candidate in candidates)
                {
                    candidateResults.Add(new CandidateResult(candidate.Name));
                }
            }

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:53175/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response;

                response = await client.GetAsync("api/Vote/results");
                if (response.IsSuccessStatusCode)
                {
                    Vote[] reports = await response.Content.ReadAsAsync<Vote[]>();
                    foreach (var report in reports)
                    {
                        foreach (var vote in votes)
                        {
                            if (report.PESEL == vote.PESEL)
                            {
                                alreadyVoted = true;
                            }
                        }

                        if (!alreadyVoted)
                        {
                            votes.Add(report);
                        }
                    }
                }
                await client.DeleteAsync("api/Vote");
            }
            VoteOperationsJSON.SerializeAllVotes(votes);

            foreach (var vote in votes)
            {
                foreach (var candidate in candidateResults)
                {
                    if (candidate.Name == vote.VoteOnCandidate)
                    {
                        candidate.Result++;
                    }
                }
            }

            foreach (var candidate in candidateResults)
            {
                Console.WriteLine($"{candidate.Name} got: {candidate.Result} votes");
            }
        }
    }
}
