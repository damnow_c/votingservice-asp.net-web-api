﻿namespace CommisionApp
{
    public class CandidateResult
    {
        public string Name { get; set; }
        public int Result { get; set; }

        public CandidateResult(string name)
        {
            Name = name;
            Result = 0;
        }
    }
}
